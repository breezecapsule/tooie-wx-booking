grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility
    repositories {        
        grailsCentral()
        mavenCentral()
        grailsRepo "http://grails.org/plugins"
        mavenRepo "http://repo.grails.org/grails/repo/"
        mavenRepo "http://repository.codehaus.org"
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"
        mavenRepo "http://artifactory.tooieapp.com/artifactory/tooie/"
        
    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

        // runtime 'mysql:mysql-connector-java:5.1.21'
    }

    plugins {
        build(":tomcat:$grailsVersion",
              ":release:2.2.1",
              ":rest-client-builder:1.0.3") {
            export = false
        }
        if(System.getProperty('tooie.plugin.deploy')) {
            println "using published tooie plugins"
            compile ":tooie-core-api:0.1.4"
        }
    }

    if(System.getProperty('tooie.plugin.deploy') && System.getProperty('tooie.plugin.deploy').equals("false")) {
        println "using local plugins"
        grails.plugin.location.'tooie-core-api' = '../tooie-core-api'
    }

}

grails.project.repos.default = "tooie"

grails {
    project {
        repos {
            tooie {
                url = "http://artifactory.tooieapp.com/artifactory/tooie/"
                username = "tooie"
                password = "triiples1601"
            }
        }
    }
}