modules = {
    jqtwo {
        resource url: 'js/jquery-2.0.3.min.js'
    }
    booking{
        dependsOn 'jqtwo'
        resource url: 'css/booking/booking.css'
        resource url: 'js/booking/BookingView.js'
    }
}