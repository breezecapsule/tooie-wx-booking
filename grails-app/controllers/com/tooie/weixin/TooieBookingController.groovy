package com.tooie.weixin

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject



class TooieBookingController {
    def restfulBookingService
    def coreApiService

    def index(long bId, String code) {
        log.info "parms ${params}"
        log.info "account receive callback from weixin bId : ${bId}"
        def user = coreApiService.findTooieWeixinUser(bId, code)
        [rId: user?.restaurantId, tooieWxUserId: user?.wxUserId, rPrefix: user?.rPrefix]
    }

    def resources(long bId, String rPrefix){
        def orgPrefix = rPrefix

        if(!orgPrefix) {
            def rInfo = coreApiService.getOrganizationInfo(bId).info
            orgPrefix = rInfo.prefix + '-' + rInfo.id
        }

        def resources = restfulBookingService.findAllBookingResourceByOrganization(orgPrefix)

        render([
                resources   : resources.collect{ JSONObject it ->
                    return it.put("deliveryHours",(it.startHour..it.endHour))
                }
        ] as JSON)
    }
}
