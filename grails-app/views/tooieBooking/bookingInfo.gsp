<%--
  Created by IntelliJ IDEA.
  User: Tony
  Date: 15-9-17
  Time: 下午2:41
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>我的预约</title>
    <r:require modules="booking"/>
    <r:layoutResources/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="format-detection" content="telephone=no"/>
</head>

<body>
<div class="info-title">
    <span>个人信息</span>
</div>
<div class="booking-info">
    <div class="booking-name booking-underline">
        <label>姓名:</label>
        <span>汤文杰</span>
        <span>先生</span>
    </div>

    <div class="booking-phone">
        <label>电话:</label>
        <span>13407155088</span>
    </div>
</div>
<div class="info-order">
    <span>订单信息</span>
</div>
<div class="booking-desc">
    <div class="booking-data booking-underline">
        <label>预约日期:</label>
        <span>2015/05/26</span>
    </div>

    <div class="booking-time booking-underline">
        <label>预约时间:</label>
        <span>17:30</span>
    </div>

    <div class="booking-table booking-underline">
        <label>预约餐台:</label>
        <span>4人桌</span>
    </div>

    <div class="booking-description">
        <label>备注信息:</label>
        <span></span>
    </div>
</div>

<div class="booking-cancel">
    <button class="cancel" disabled>取消预约</button>
</div>
<div class="booking-confirm" hidden>
    <div class="confirm-body">
        <span>预约完成，请等待确认!</span>
        <button>OK</button>
    </div>
</div>
<r:layoutResources/>
</body>
</html>