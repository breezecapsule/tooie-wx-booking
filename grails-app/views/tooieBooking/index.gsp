<%--
  Created by IntelliJ IDEA.
  User: Tony
  Date: 15-9-15
  Time: 下午2:46
--%>

<%@ page import="grails.util.GrailsUtil" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>我的预约</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="format-detection" content="telephone=no"/>
</head>

<body>

<r:layoutResources/>

<input id="rId" type="hidden" value="${rId}"/>
<input id="rPrefix" type="hidden" value="${rPrefix}"/>
<input id="tooieWxUserId" type="hidden" value="${tooieWxUserId}"/>

<div id="root"></div>
<script src="../js/dist/wxbooking.bundle.js"></script>
%{--<g:if test="${grails.util.GrailsUtil.environment.startsWith("production") || grails.util.GrailsUtil.environment == 'uat'}">
<script src="../js/dist/wxbooking.bundle.js"></script>
</g:if>
<g:else>
    <script src="https://localhost:3002/dev/wxbooking.bundle.js"></script>
</g:else>--}%
</body>
</html>