'use strict';

import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import Immutable from 'immutable';
import createLogger from 'redux-logger';
import { devTools, persistState } from 'redux-devtools';


function combineImmutableReducers(reducers) {
    var combined_reducers = combineReducers(reducers);
    return (state, action) => Immutable.Map(combined_reducers(
        Immutable.Map.isMap(state) ? state.toObject() : state, action
    ));
}

const logger = createLogger({
    stateTransformer (state) {
        return state.toJS();
    }
});

export default function (reducers) {
    const reducer = combineImmutableReducers(reducers);
    /**
     * DEV MODE
     */
    if (__DEV__) {
        return compose(
            applyMiddleware(thunk, logger),
            devTools()
        )(createStore)(reducer);
    } else {
        return compose(
            applyMiddleware(thunk)
        )(createStore)(reducer);
    }
}