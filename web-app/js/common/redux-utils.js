'use strict';

import { Provider, connect } from 'react-redux';
import React from 'react';
import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';

export function connectStore(Component, store) {
    const ConnectedComponent = connect((state) => {
        return {"store": state}
    })(Component);
    return class extends React.Component {
        render() {
            return (
                <div>
                    <Provider store={store}>
                        <ConnectedComponent {...this.props}/>
                    </Provider>
                </div>
            )
        }
    }
}
