'use strict';

import _ from 'underscore';

function createHandler(holder, key, handler) {
    return function(args) {
        // redux will provide a dispatcher and the current state.
        return (dispatch, state) => {
            if (handler == SimpleAction) {
                return dispatch({type: key, args: args});
            } else {
                return new Promise((resolve, reject) => {
                    let meFunc = function(args) {
                        return {type: key, args: args};
                    };
                    let dispatchFunc = (action) => {
                        return new Promise((resolve, reject) => {
                            let result = dispatch(action);
                            if (result && typeof(result.then) === 'function' && typeof(result.catch) === 'function') {
                                // result is a promise, chain the promise.
                                result.then(resolve).catch(reject);
                            } else {
                                // auto resolve because result is not a promise.
                                resolve();
                            }
                        });
                    };
                    let dispatchMeFunc = (args) => {
                        return dispatchFunc(meFunc(args));
                    };
                    let updateHistoryFunc = (path, query, state) => {
                        console.log("updateHistory", path, query);
                        history.pushState(state, path, query);
                        return Promise.resolve();
                    };
                    let result = handler.call({
                        Actions: holder.Actions,
                        Keys: holder.Keys,
                        me: meFunc,
                        dispatch: dispatchFunc,
                        updateHistory: updateHistoryFunc,
                        currentState: state,
                        dispatchMe: dispatchMeFunc
                    }, args);
                    if (result && typeof(result.then) === 'function' && typeof(result.catch) === 'function') {
                        // result is a promise, chain the promise.
                        result.then(resolve).catch(reject);
                    } else {
                        // auto resolve because result is not a promise.
                        resolve();
                    }
                });
            }
        };
    }
}

export function buildActions(actions) {
    let holder = {
        Actions: {},
        Keys: {}
    };
    _(actions).chain().keys().each((key) => {
        holder.Keys[key] = key;
        holder.Actions[key] = createHandler(holder, key, actions[key]);
        // create 'Completed' auto-action if not defined.
        let completedActionName = key + "Completed";
        if (_(actions).keys().indexOf(completedActionName) < 0) {
            holder.Keys[completedActionName] = completedActionName;
            holder.Actions[completedActionName] = createHandler(holder, completedActionName, SimpleAction);
        }
        // create 'Failed' auto-action if not defined.
        let failedActionName = key + "Failed";
        if (_(actions).keys().indexOf(failedActionName) < 0) {
            holder.Keys[failedActionName] = failedActionName;
            holder.Actions[failedActionName] = createHandler(holder, failedActionName, SimpleAction);
        }
        // create 'InProgress' auto-action if not defined.
        let inProgressActionName = key + "InProgress";
        if (_(actions).keys().indexOf(inProgressActionName) < 0) {
            holder.Keys[inProgressActionName] = inProgressActionName;
            holder.Actions[inProgressActionName] = createHandler(holder, inProgressActionName, SimpleAction);
        }
    }).values();
    return holder;
}

export function SimpleAction(data) {
    return {type: this.key, ...data};
}