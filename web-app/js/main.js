'use strict';
import queryParser from "query-string";
//
require('es6-promise').polyfill();

// start app
import css from './less/app.less';
import createImmutableStore from './common/immutable-store';
import {connectStore} from './common/redux-utils';
import * as reducers from './reducers/reducers';
import App from './components/App';
import React from 'react';
import ReactDOM from 'react-dom';
import { connect, Provider } from 'react-redux';
//import BookingActions from './actions/BookingActions';
import i18n from "i18next-client";
import resBundle from "./locales/locales";

//mixpanel script
(function (f, b) {
    if (!b.__SV) {
        var a, e, i, g;
        window.mixpanel = b;
        b._i = [];
        b.init = function (a, e, d) {
            function f(b, h) {
                var a = h.split(".");
                2 == a.length && (b = b[a[0]], h = a[1]);
                b[h] = function () {
                    b.push([h].concat(Array.prototype.slice.call(arguments, 0)))
                }
            }

            var c = b;
            "undefined" !== typeof d ? c = b[d] = [] : d = "mixpanel";
            c.people = c.people || [];
            c.toString = function (b) {
                var a = "mixpanel";
                "mixpanel" !== d && (a += "." + d);
                b || (a += " (stub)");
                return a
            };
            c.people.toString = function () {
                return c.toString(1) + ".people (stub)"
            };
            i = "disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
            for (g = 0; g < i.length; g++)f(c, i[g]);
            b._i.push([a, e, d])
        };
        b.__SV = 1.2;
        a = f.createElement("script");
        a.type = "text/javascript";
        a.async = !0;
        a.src = "undefined" !== typeof MIXPANEL_CUSTOM_LIB_URL ? MIXPANEL_CUSTOM_LIB_URL : "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";
        e = f.getElementsByTagName("script")[0];
        e.parentNode.insertBefore(a, e);
    }
})(document, window.mixpanel || []);

const ENV = queryParser.parse(window.location.search).env;

window._mp = function(action, props) {
    if(!action) {
        return;
    }

    if(!props) {
        props = {};
    }

    props.env = ENV;

    mixpanel.track(action, props);
    console.log('send action ' + action + " to mixpanel", props);
};

i18n.init({detectLngQS: 'lang', fallbackLng: 'zh_CN', resStore: resBundle}, () => {
    //window.document.title = i18n.t('header.title.app');
    const store = createImmutableStore(reducers);
    const ConnectedApp = connectStore(App, store);

    if(mixpanel) {
        console.log("mixpanel initialized");
        mixpanel.init("467ca97eba88f9d291f0f1edebcf5c8f");
    }
    ReactDOM.render(
        <ConnectedApp/>,
        document.getElementById('root')
    );
});

