'use strict';

import {buildActions, SimpleAction} from '../common/action-builder';
//import TableActions from './TableActions';
//import MenuActions from './MenuActions.js';
import BookingActions from './BookingActions'
//import config from '../config/config';
import $ from 'jquery';
import i18n from "i18next-client";
import _ from 'underscore';
import io from 'socket.io-client';

const clockTickInterval = 60 * 1000; // 60 seconds a tick

let websocket = null;

export default buildActions({
    InitialiseWebsocket(args) {
        let {pushServer, pushChannelName} = args;
        return this.dispatchMe(pushServer, pushChannelName)
            .then(() => {
                return new Promise((resolve, reject) => {
                    resolve();
                    console.log("connecting to websocket", pushServer, pushChannelName);
                    websocket = io(pushServer + '/tooie-' + pushChannelName, {
                        multiplex: false,
                        timeout: 10 * 1000
                    });
                    websocket.on('connect', () => {
                        console.log("websocket connected");
                    });
                    websocket.on("tooie-" + pushChannelName, (msg) => {
                        msg = JSON.parse(msg);
                        switch (msg.event) {
                            case "TableBillChangeEvent":
                                this.dispatch(TableActions.Actions.BillChanged(msg.payload));
                            default:
                                console.log("received msg", msg);
                        }
                    });
                    websocket.on('reconnect', () => {
                        console.log('websocket reconnecting');
                    });
                    websocket.on('disconnect', (err) => {
                        console.log('websocket disconnected');
                    });
                });
            });
    },
    Busy() {
        return this.dispatchMe();
    },
    NotBusy() {
        return this.dispatchMe();
    },
    OpenListBookings() {
        return this.dispatchMe()
            .then(() => this.dispatch(BookingActions.Actions.GetMyBookingsList()))
            .catch((error)=>{
                console.log(error);
                return this.dispatchMe();
            });
    },
    OpenInvalidListBookings() {
        return this.dispatchMe()
            .then(() => this.dispatch(BookingActions.Actions.GetMyInvalidBookingsList()))
            .catch((error)=>{
                console.log(error);
                return this.dispatchMe();
            });
    },
    OpenNewBooking(args) {
        return this.dispatchMe()
            .then(() => this.dispatch(BookingActions.Actions.InitialNewBooking(args)));
    },
    OpenModifyBooking(args) {
        return this.dispatchMe()
            .then(() => this.dispatch(BookingActions.Actions.ModifyBooking(args)));
    },
    OpenDetailsBooking(args) {
        return this.dispatchMe()
            .then(() => this.dispatch(BookingActions.Actions.showSelectedBooking(args)));
    }
});

