'use strict';

import {buildActions, SimpleAction} from '../common/action-builder';
import UIActions from './UIActions';
import $ from 'jquery';
import i18n from "i18next-client";
import _ from 'underscore';
import io from 'socket.io-client';
import queryString from 'query-string';

export default buildActions({
    Bootstrap(args) {
        return this.dispatchMe(args)
            .then(()=> initailWechatUser(args))
            .then(() => this.dispatch(this.Actions.GetMyBookingsList()))
            .catch((error)=>{
                console.log(error);
                alter("页面初始化失败，请重新打开我的预定页面");
                return this.dispatchMe();
            });
    },
    GetMyBookingsList(args) {
        return this.dispatchMe()
            .then(() => this.dispatch(UIActions.Actions.Busy()))
            .then(() => getEffectiveBookings())
            .then((data) =>{
                    this.dispatch(UIActions.Actions.NotBusy())
                        .then(()=> this.dispatch(this.Actions.GetMyBookingsListCompleted(data)));
            })
            .catch((error)=>{
            console.log(error);
            return this.dispatch(UIActions.Actions.NotBusy())
                .then(()=>this.dispatch(this.Actions.GetMyBookingsListFailed()));
        });
    },
    GetMyInvalidBookingsList(args) {
        return this.dispatchMe()
            .then(() => this.dispatch(UIActions.Actions.Busy()))
            .then(() => getInvalidBookings())
            .then((data) =>{
                    this.dispatch(UIActions.Actions.NotBusy())
                        .then(()=> this.dispatch(this.Actions.GetMyInvalidBookingsListCompleted(data)));
            })
            .catch((error)=>{
            console.log(error);
            return this.dispatch(UIActions.Actions.NotBusy())
                .then(()=>this.dispatch(this.Actions.GetMyBookingsListFailed()));
        });
    },
    InitialNewBooking(args) {
        return this.dispatchMe()
            .then(() => this.dispatch(UIActions.Actions.Busy()))
            .then(() => {
                this.dispatch(this.Actions.GetResources(args))
                    .then(()=>this.dispatch(UIActions.Actions.NotBusy()))
                }
            )
            .catch((error)=>{
                console.log(error);
                alter("预定页面初始化失败，请重新打开预定页面")
                return this.dispatchMe()
                    .then(()=>this.dispatch(UIActions.Actions.NotBusy()));
            });
    },
    GetResources(args) {
        return this.dispatchMe()
            .then(() => this.dispatch(UIActions.Actions.Busy()))
            .then(()=>getResources(args))
            .then((data) =>{
                    this.dispatch(UIActions.Actions.NotBusy())
                        .then(()=> this.dispatch(this.Actions.GetResourcesCompleted(data)));
            }).catch((error)=>{
            console.log(error);
            return this.dispatch(UIActions.Actions.NotBusy())
                .then(()=>this.dispatch(this.Actions.NewBookingFailed()));
        });
    },
    showSelectedBooking(args) {
        return this.dispatchMe(args);
    },
    NewBooking(args) {
        return this.dispatchMe()
            .then(() => this.dispatch(UIActions.Actions.Busy()))
            .then(()=>{
                let resources = this.currentState().getIn(["booking","resources"]);
                _.filter(resources, function(resource){
                    if(resource.id==args.resourceId)
                        args.resourceName = resource.name
                });
                return createNewBooking(args);
            })
            .then((data) =>{
                this.dispatch(UIActions.Actions.NotBusy())
                    .then(()=> this.dispatch(this.Actions.NewBookingCompleted()));
            }).catch((error)=>{
                //TODO i18n
                alert("您预定的餐桌失败，请重新尝试.");
                return this.dispatch(UIActions.Actions.NotBusy())
                    .then(()=>this.dispatch(this.Actions.NewBookingFailed()));
        });
    },
    CancelBooking(args) {
        return this.dispatchMe()
            .then(() => this.dispatch(UIActions.Actions.Busy()))
            .then(()=>{
                return cancelBooking(args);
            })
            .then((data) =>{
                this.dispatch(UIActions.Actions.NotBusy())
                    .then(()=> this.dispatch(this.Actions.CancelBookingCompleted()))
                    .then(()=> this.dispatch(UIActions.Actions.OpenListBookings()));
            }).catch((error)=>{
                //TODO i18n
                alert("取消预约失败，请重新尝试.");
                return this.dispatch(UIActions.Actions.NotBusy())
                    .then(()=>this.dispatch(this.Actions.CancelBookingFailed()));
        });
    },
    ModifyBooking(args) {
        return this.dispatchMe()
            .then(() => this.dispatch(UIActions.Actions.Busy()))
            .then(() => {
                this.dispatch(this.Actions.GetResources({bId:args.bId}))
                    .then(()=>this.dispatch(this.Actions.ModifyBookingCompleted(args.booking)))
                    .then(()=>this.dispatch(UIActions.Actions.NotBusy()))
            }
        )
            .catch((error)=>{
                console.log(error);
                alter("修改页面初始化失败")
                return this.dispatchMe()
                    .then(()=>this.dispatch(UIActions.Actions.NotBusy()));
            });
    },
    ConfirmBookingSuccessed() {
        return this.dispatchMe();
    },
    SelectGender(args) {
        return this.dispatchMe(args);
    },
    onSelectTable(args) {
        return this.dispatchMe(args);
    },
    EditingName(args) {
        return this.dispatchMe(args);
    },
    EditingPhone(args) {
        return this.dispatchMe(args);
    },
    EditingComment(args) {
        return this.dispatchMe(args);
    },
    SelectDate(args) {
        return this.dispatchMe(args);
    },
    SelectTime(args) {
        return this.dispatchMe(args);
    },
    SelectHour(args) {
        return this.dispatchMe(args);
    },
    SelectMinutes(args) {
        return this.dispatchMe(args);
    },
    GetTables(args) {
        return this.dispatchMe(args);
    },
    SelectTable(args) {
        return this.dispatchMe(args);
    },
    EdittingTable(args) {
        return this.dispatchMe(args);
    }
});

function initailWechatUser(args){
    return $.get("/TooieWeixin/initialTooieWeixinUser",args);
}

function getEffectiveBookings(){
    var args = {}, userId = $('#tooieWxUserId').val();

    if(userId) {
        args['tooieWxUserId'] = userId;
    }

    return $.post("/booking/getEffectiveBookings", args);
}

function getInvalidBookings(){
    var args = {}, userId = $('#tooieWxUserId').val();

    if(userId) {
        args['tooieWxUserId'] = userId;
    }

    return $.post("/booking/getInvalidBookings", args);
}

function getResources(args){
    var rPrefix = $('#rPrefix').val();

    if(rPrefix) {
        args.rPrefix = rPrefix;
    }

    return $.get("/TooieBooking/resources",args);
}

function createNewBooking(args){
    var id = $('#tooieWxUserId').val();

    if(id) {
        args.tooieWxUserId = id;
    }

    return $.post("/booking/saveBooking",args);
}

function cancelBooking(args){
    return $.get("/booking/cancelBooking",args);
}
