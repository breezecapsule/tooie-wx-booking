var path = require('path');
var webpack = require('webpack');
var fs = require("fs");
//TODO unfinished configuration
module.exports = {
    entry: [
        './main'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'wxbooking.bundle.js',
        publicPath: "/js/dist/"
    },
    plugins: [
        new webpack.DefinePlugin({'process.env.NODE_ENV': '"production"', '__DEV__': false}),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: false,
            compress: {
                warnings: false
            }
        }),
        function() {
            this.plugin("done", function(stats) {
                fs.writeFileSync('./dist/hash', stats.hash);
            });
        }
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader'],
                include: path.join(__dirname, './'),
                exclude: path.resolve(__dirname, "node_modules")
            },
            { test: /\.less$/, loader: "style-loader!css-loader?modules!less"
            },
            {
                test: /\.css$/, loader: "style-loader!css-loader?modules",
                exclude: path.resolve(__dirname, "node_modules")
            },
            {
                test: /\.css$/, loader: "style-loader!css-loader?",
                include: path.resolve(__dirname, "node_modules")
            },
            {test: /\.woff$/, loader: "url-loader?limit=10000&minetype=application/font-woff"},
            {test: /\.woff2$/, loader: "url-loader?limit=10000&minetype=application/font-woff"},
            {test: /\.ttf$/, loader: "file-loader"},
            {test: /\.eot$/, loader: "file-loader"},
            {test: /\.svg$/, loader: "file-loader"},
            {test: /\.png/, loader: "file-loader"},
            {test: /\.gif$/, loader: "file-loader"},
            {test: /\.jpg$/, loader: "file-loader"}
        ]
    }
};
