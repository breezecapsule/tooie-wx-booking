'use strict';

import Immutable from 'immutable';
import UIActions from '../actions/UIActions';
import BookingActions from '../actions/BookingActions';
import uuid from 'node-uuid';
import _ from 'underscore';
import moment from 'moment';

const INITIAL_UI_STATE = {

    busy: false,
    error: null,
    "page": "list"
};

export function ui(state, action) {
    state = state || Immutable.Map(INITIAL_UI_STATE);
    switch (action.type) {
        case UIActions.Keys.Busy:
            console.log("busy",state.getIn(["busy"]));
            return state.merge({
                busy: true
            });
        case UIActions.Keys.NotBusy:
            console.log("busy",state.getIn(["busy"]));
            return state.merge({
                busy: false
            });
        case UIActions.Keys.OpenListBookings:
            return state.merge({"page": "list"});
        case UIActions.Keys.OpenInvalidListBookings:
            return state.merge({"page": "invalidList"});
        case UIActions.Keys.OpenNewBooking:
            return state.merge({"page": "new"});
        case UIActions.Keys.OpenDetailsBooking:
            return state.merge({"page": "details"});
        case UIActions.Keys.OpenModifyBooking:
            return state.merge({"page": "modify"});
        default:
            return state;
    }
}

const INITIAL_BOOKING_STATE = {
    bId:null,
    id:null,
    fullname: "",
    male: true,
    mobile: "",
    bookingDate: moment(moment().add('day',1)).format("YYYY-MM-DD"),
    bookingTime:"",
    bookingHour:"12",
    bookingMinutes:"00",
    resourceId:-1,
    memo:"",
    inBooking:true,
    bookingSuccessed:false,
    showBookingSuccessed:false,
    resources:[],
    hours:[12]
};

export function booking(state, action) {
    state = state || Immutable.Map(INITIAL_BOOKING_STATE);
    switch (action.type) {
        case BookingActions.Keys.Bootstrap:
            return state.merge({
                bId: action.args.bId
            });
        case BookingActions.Keys.EditingName:
            return state.merge({
                fullname: action.args
            });
        case BookingActions.Keys.EditingPhone:
            return state.merge({
                mobile: action.args
            });
        case BookingActions.Keys.EditingComment:
            return state.merge({
                memo: action.args
            });
        case BookingActions.Keys.SelectGender:
            return state.merge({
                male: action.args
            });
        case BookingActions.Keys.onSelectTable:
            const resourceId = action.args;
            const resource = state.getIn(['resources']).find((resource) => resource.id == resourceId);
            return state.merge({
                resourceId: resourceId,
                hours: resource.deliveryHours
            });
        case BookingActions.Keys.SelectDate:
            return state.merge({
                bookingDate: action.args
            });
        case BookingActions.Keys.SelectTime:
            return state.merge({
                bookingTime: action.args
            });
        case BookingActions.Keys.SelectHour:
            return state.merge({
                bookingHour: action.args
            });
        case BookingActions.Keys.SelectMinutes:
            return state.merge({
                bookingMinutes: action.args
            });
        case BookingActions.Keys.NewBooking:
            return state.merge({
                inBooking: true
            });
        case BookingActions.Keys.NewBookingFailed:
            return state.merge({
                inBooking: false
            });
        case BookingActions.Keys.NewBookingCompleted:
            return state.merge({
                inBooking: false,
                bookingSuccessed: true,
                showBookingSuccessed: true
            });
        case BookingActions.Keys.ConfirmBookingSuccessed:
            return state.merge({
                showBookingSuccessed: false
            });
        case BookingActions.Keys.GetResources:
            state =  state.setIn(["id"], null);
            state =  state.setIn(["male"], true);
            state =  state.setIn(["fullname"], "");
            state =  state.setIn(["mobile"], "");
            state =  state.setIn(["bookingDate"], moment(moment().add('day',1)).format("YYYY-MM-DD"));
            state =  state.setIn(["bookingTime"],"");
            state =  state.setIn(["bookingHour"],"12");
            state =  state.setIn(["bookingMinutes"],"00");
            state =  state.setIn(["resourceId"], -1);
            state =  state.setIn(["memo"], "");
            state =  state.setIn(["bookingSuccessed"], false);
            state =  state.setIn(["showBookingSuccessed"], false);
            return state.setIn(["inBooking"], true);
        case BookingActions.Keys.GetResourcesCompleted:
            state =  state.setIn(["resources"], action.args.resources);
            return state.setIn(["inBooking"], false);
        case BookingActions.Keys.ModifyBookingCompleted:
            const rId = action.args.resourceId;
            const rs = state.getIn(['resources']).find((resource) => resource.id == rId);
            state =  state.setIn(["id"], action.args.id);
            state =  state.setIn(["male"], action.args.male);
            state =  state.setIn(["fullname"], action.args.fullname);
            state =  state.setIn(["mobile"], action.args.mobile);
            state =  state.setIn(["bookingDate"], action.args.bookingDate);
            state =  state.setIn(["bookingTime"],action.args.bookingTime);
            state =  state.setIn(["bookingHour"],action.args.bookingHour);
            state =  state.setIn(["hours"],rs.deliveryHours);
            state =  state.setIn(["bookingMinutes"],action.args.bookingMinutes);
            state =  state.setIn(["resourceId"], rId);
            state =  state.setIn(["memo"], action.args.memo);
            state =  state.setIn(["bookingSuccessed"], false);
            state =  state.setIn(["showBookingSuccessed"], false);
            return state.setIn(["inBooking"], false);
        default:
            return state;
    }
}
const INITIAL_MYBOOKING_STATE = {
    myBookings:Immutable.List(),
    myInvalidBookings:Immutable.List(),
    selectedBooking: Immutable.Map()
};

export function myBooking(state, action) {
    state = state || Immutable.Map(INITIAL_MYBOOKING_STATE);
    switch (action.type) {
        case BookingActions.Keys.GetMyBookingsListCompleted:
            return state.merge({
                "myBookings": Immutable.List(action.args)
            });
        case BookingActions.Keys.GetMyInvalidBookingsListCompleted:
            return state.merge({
                "myInvalidBookings": Immutable.List(action.args)
            });
        case BookingActions.Keys.showSelectedBooking:
            return state.merge({
                "selectedBooking": Immutable.Map(action.args)
            });
        default:
            return state;
    }
}