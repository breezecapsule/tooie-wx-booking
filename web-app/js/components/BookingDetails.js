'use strict';

import React from 'react';
import {Grid, Col, Row, Button, Modal, Input, Alert, Panel} from 'react-bootstrap';
import BusySpinner from './BusySpinner';
import cn from 'classnames';
import BookingActions from '../actions/BookingActions';
import UIActions from '../actions/UIActions';
import styles from '../less/app.less';
import queryString from 'query-string';
import _ from 'underscore';
import moment from 'moment';

export default class extends React.Component {
    render() {
        const {store, dispatch} = this.props;
        const booking = store.getIn(["myBooking","selectedBooking"]).toObject();
        let user = booking.user, resource = booking.resource, bookingStatus = booking.bookingStatus;
        let modify = false;
        if(bookingStatus!=null&&bookingStatus.name == "Unconfirmed"){
                modify = true;
            if(booking.uiInvalid)
                modify = false;
        }

        let name = user!=null?user.fullname:"";
        let mobile = user!=null?user.mobile:"";
        let table = resource!=null?resource.name:"";
        let date = booking!=null?moment(booking.startDate).format("YYYY-MM-DD"):"";
        let time = booking!=null?moment(booking.startDate).format("HH:mm"):"";
        let comment = booking!=null?booking.memo:"";
        return (
            <div className={cn(styles.myBookingDetails)}>
                <div className={cn(styles.detailsBackward)} onClick={this.handleBackward.bind(this)} >
                    <button >返回</button>
                </div>
                <div className={styles.header}>
                    <p><img src="../images/profile.png" /> 个人信息</p>
                </div>

                <div className={cn(styles.bookingInfo,styles.booking)}>
                    <div className={cn(styles.bookingName,styles.bookingUnderline)}>
                        {"姓名："+ name}
                    </div>

                    <div className={cn(styles.bookingPhone)}>
                        {"电话："+ mobile}
                    </div>
                </div>
                <div className={styles.bookingDescHeader}>
                    <p><img src="../images/booking-list.png" />订单信息</p>
                </div>

                <div className={styles.bookingDesc}>
                    <div className={cn(styles.bookingData,styles.bookingUnderline)}>
                        {"预约日期："+date}
                    </div>

                    <div className={cn(styles.bookingTime, styles.bookingUnderline)}>
                        {"预约时间："+time}
                    </div>

                    <div className={cn(styles.bookingTable, styles.bookingUnderline)}>
                        {"预约餐台："+table}
                    </div>

                    <div className={cn(styles.bookingDescription)} >
                        {"备注：" + (comment==null?"无":comment)}
                    </div>
                </div>

                <div className={cn(styles.modifyBooking,!modify?styles.disabled:"")} onClick={this.handleModifyBooking.bind(this)} >
                    <button >修改</button>
                </div>

            </div>
        );
    }
    handleBackward(){
        const booking = this.props.store.getIn(["myBooking","selectedBooking"]).toObject();
        if(booking!=null&&!booking.uiInvalid)
            this.props.dispatch(UIActions.Actions.OpenListBookings());
        else
            this.props.dispatch(UIActions.Actions.OpenInvalidListBookings());
    }
    handleModifyBooking(){
        const booking = this.props.store.getIn(["myBooking","selectedBooking"]).toObject();
        let bId = this.props.store.getIn(["booking"]).toObject().bId;
        let modifyBooking ={
            id:booking.id,
            fullname: booking.user.fullname,
            male: booking.user.gender=="MALE"?true:false,
            mobile: booking.user.mobile,
            bookingDate: moment(booking.startDate).format("YYYY-MM-DD"),
            bookingTime: moment(booking.startDate).format("HH:mm"),
            bookingHour: moment(booking.startDate).format("HH"),
            bookingMinutes: moment(booking.startDate).format("mm"),
            resourceId:booking.resource.id,
            memo:booking.memo
        };

        this.props.dispatch(UIActions.Actions.OpenModifyBooking({booking:modifyBooking,bId:bId}));
    }
}

