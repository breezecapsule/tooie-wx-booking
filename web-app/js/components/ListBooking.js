'use strict';

import React from 'react';
import {Grid, Col, Row, Button, Modal, Input, Alert, Panel} from 'react-bootstrap';
import BusySpinner from './BusySpinner';
import cn from 'classnames';
import BookingActions from '../actions/BookingActions';
import UIActions from '../actions/UIActions';
import styles from '../less/app.less';
import queryString from 'query-string';
import _ from 'underscore';
import moment from 'moment';

export default class extends React.Component {
    render() {
        const {store, dispatch} = this.props;
        const myBookings = store.getIn(["myBooking"]).toObject().myBookings.toArray();
        let connent = [
            <div className={styles.myBooking}>
                <p>没有预约</p>
            </div>
        ];
        if(myBookings.length>0){
            connent = [];
            myBookings.map((booking)=>{
                let status = "";
                let statusStyles =  styles.bookingStatusUnconfirm;
                let cancel = false;
                switch (booking.bookingStatus.name) {
                    case "Accept":
                        status = "已确认";
                        statusStyles = styles.bookingStatusAccept;
                        break;
                    case "Unconfirmed":
                        status = "未确认";
                        statusStyles = styles.bookingStatusUnconfirm;
                        cancel = true;
                        break;
                    case "Refused":
                        status = "已拒绝";
                        statusStyles = styles.bookingStatusReject;
                        break;
                    default:
                        status = "未知"
                }
                let date = moment(booking.startDate).format("YYYY年MM月DD日");
                let time = moment(booking.startDate).format("HH:mm");
                connent.push(
                    <div className={styles.myBooking} key={myBookings.id}>
                        <div className={cn(styles.bookingData,styles.bookingUnderline)}>
                            <img src="../images/calendar.png" />
                            <span className={styles.bookingDate}>{date}</span>
                            <span className={styles.bookingTime}>{time}</span>
                            <span className={statusStyles}>状态: {status}</span>
                        </div>

                        <div className={cn(styles.bookingDetails)}>
                            <div className={styles.bookingRestaurant}>{booking.organizationName}</div>
                            <div className={styles.bookingTable}>{booking.resource.name}</div>
                            <div className={styles.bookingComment}>备注:{booking.memo}</div>
                        </div>

                        <div data-id={booking.id} className={styles.bookingOpenDetails} onClick={this.showBookingDetails.bind(this)}>查看详情</div>
                        <div data-id={booking.id} className={cn(styles.bookingCancelButton,!cancel?styles.disabled:"")} onClick={this.handleCancelBooking.bind(this)}>取消预约</div>

                    </div>
                )
            });
        }
        return (
            <div className={styles.myBookings}>
                {connent}
                <div className={cn(styles.newBooking)} onClick={this.createNewBooking.bind(this)} >
                    <button>添加预约</button>
                </div>
                <div className={cn(styles.invalidBooking)} onClick={this.showInvalidBooking.bind(this)} >
                    查看已过期预约 >>
                </div>
            </div>
        );
    }
    createNewBooking(){
        let bId = this.props.store.getIn(["booking"]).toObject().bId;
        this.props.dispatch(UIActions.Actions.OpenNewBooking({bId:bId}));
    }
    showInvalidBooking(){
        this.props.dispatch(UIActions.Actions.OpenInvalidListBookings());
    }
    showBookingDetails(event){
        let id = event.target.getAttribute("data-id");
        let myBookings = this.props.store.getIn(["myBooking"]).toObject().myBookings.toArray();
        let myBooking = _.find(myBookings, function(myBooking){
            if(myBooking.id == id)
                return myBooking;
        });
        myBooking.uiInvalid = false;
        myBooking.male = myBooking.user.gender=="MALE"?true:false;
        this.props.dispatch(UIActions.Actions.OpenDetailsBooking(myBooking));
    }
    handleCancelBooking(event){
        if(confirm("确认删除该预约")){
            let id = event.target.getAttribute("data-id");
            this.props.dispatch(BookingActions.Actions.CancelBooking({id: id}));
        }
    }
}

