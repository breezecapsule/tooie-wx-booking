'use strict';

import React from 'react';
import {Grid, Col, Row, Button, Modal, Input, Alert, Panel} from 'react-bootstrap';
import BusySpinner from './BusySpinner';
import cn from 'classnames';
import BookingActions from '../actions/BookingActions';
import UIActions from '../actions/UIActions';
import styles from '../less/app.less';
import queryString from 'query-string';
import _ from 'underscore';
import moment from 'moment';

export default class extends React.Component {
    render() {
        const {store, dispatch} = this.props;
        const booking = store.getIn(["booking"]).toObject();
        let resourcesOptions = [];
        booking.resources.map(function(resource) {
            resourcesOptions.push(<option value={resource.id}>{resource.name}</option>)
        });


        let hourOptions = [];
        booking.hours.map(function(hour) {
            hourOptions.push(<option value={hour}>{hour}</option>)
        });

        let minutesOptions = [];
        ["00", "15", "30", "45"].map(function(minutes) {
            minutesOptions.push(<option value={minutes}>{minutes}</option>)
        });

        return (
            <Grid fluid>
                <Row>
                    <Col xs={12}>
                        <div className={cn(styles.newBookingBackward)} onClick={this.handleCancelNewBooking.bind(this)} >
                            <button >返回</button>
                        </div>
                        <div className={styles.newBookingHeader}>
                            <p><img src="../images/booking-list.png" />修改预约</p>
                        </div>
                        <div className={cn(styles.bookingInfo,styles.booking)}>
                            <div className={cn(styles.bookingName,styles.bookingUnderline)}>
                                <input type="text" name="name" className="name" placeholder="您的姓名" disabled={booking.bookingSuccessed} value={booking.fullname} onChange={this.onHandleInputName.bind(this)}/>

                                <div className={cn(styles.male,booking.male?styles.checked:"")}>
                                    <img src="/images/checked.png" className={styles.bookingChecked} onClick={()=>{this.onSelectGender(true)}}/>
                                    <img src="/images/check.png" className={styles.bookingCheck} onClick={()=>{this.onSelectGender(true)}}/>
                                    <label>先生</label>
                                </div>

                                <div className={cn(styles.female,booking.male?"":styles.checked)}>
                                    <img src="/images/checked.png" className={styles.bookingChecked} onClick={()=>{this.onSelectGender(false)}}/>
                                    <img src="/images/check.png" className={styles.bookingCheck} onClick={()=>{this.onSelectGender(false)}}/>
                                    <label>女士</label>
                                </div>
                            </div>

                            <div className={cn(styles.bookingPhone)}>
                                <input type="tel" name="phone" className="phone" value={booking.mobile} placeholder="您的电话" disabled={booking.bookingSuccessed} onChange={this.onHandleInputPhone.bind(this)}/>
                            </div>
                        </div>

                        <div className={styles.bookingDesc}>

                            <div className={cn(styles.bookingTable, styles.bookingUnderline)}>
                                <img src="/images/right-arrow.png"/>
                                <select name="" ref="table" value={booking.resourceId} disabled={booking.bookingSuccessed} onChange={this.onSelectTable.bind(this)}>
                                    <option value={"-1"}>选择餐台(必须填写)</option>
                                    {resourcesOptions}
                                </select>
                            </div>

                            <div className={cn(styles.bookingData,styles.bookingUnderline)}>
                                <img src="/images/right-arrow.png"/>
                                <input type="date" name="date" ref="date" value={booking.bookingDate} disabled={booking.bookingSuccessed} onChange={this.onSelectDate.bind(this)}/>
                            </div>

                            <div className={cn(styles.bookingTime, styles.bookingUnderline)}>
                                <img src="/images/right-arrow.png"/>
                                <select name="hour" ref="hour" value={booking.bookingHour} disabled={booking.bookingSuccessed} onChange={this.onSelectHour.bind(this)}>
                                    {hourOptions}
                                </select>
                                <span>:</span>
                                <select name="minus" ref="minus" value={booking.bookingMinutes} disabled={booking.bookingSuccessed} onChange={this.onSelectMinutes.bind(this)}>
                                    {minutesOptions}
                                </select>
                            </div>

                            <div className={cn(styles.bookingDescription)} >
                                <textarea placeholder="备注(可以不填)" value={booking.memo} onChange={this.onHandleInputComment.bind(this)} disabled={booking.bookingSuccessed}></textarea>
                            </div>
                        </div>
                        <div className={cn(styles.modifyBooking)} onClick={this.handleNewBooking.bind(this)} diabled={booking.inBooking}>
                            <button className={booking.inBooking?styles.disabled:""}>修改预约</button>
                        </div>

                        <div className={cn(styles.bookingConfirm)} hidden={!booking.showBookingSuccessed} >
                            <div className={styles.confirmBody} >
                                <span>修改预约完成，请等待确认!</span>
                                <button onClick={this.onConfirmBookingSuccessed.bind(this)}>OK</button>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Grid>
        );
    }
    onHandleInputName(event){
        this.props.dispatch(BookingActions.Actions.EditingName(event.target.value));
    }
    onHandleInputPhone(event){
        this.props.dispatch(BookingActions.Actions.EditingPhone(event.target.value));
    }
    onHandleInputComment(event){
        this.props.dispatch(BookingActions.Actions.EditingComment(event.target.value));
    }
    onSelectGender(args){
        this.props.dispatch(BookingActions.Actions.SelectGender(args));
    }
    onSelectDate(event){
        let date = event.target.value;
        let now = moment(moment()).format("YYYY-MM-DD");
        if(moment(date).isBefore(now))
            date = now
        this.props.dispatch(BookingActions.Actions.SelectDate(date));
    }
    onSelectTime(event){
        console.log("onSelectTime",event.target.value);
        this.props.dispatch(BookingActions.Actions.SelectTime(event.target.value));
    }
    onSelectHour(event){
        this.props.dispatch(BookingActions.Actions.SelectHour(event.target.value));
    }
    onSelectMinutes(event){
        this.props.dispatch(BookingActions.Actions.SelectMinutes(event.target.value));
    }
    onSelectTable(event){
        this.props.dispatch(BookingActions.Actions.onSelectTable(event.target.value));
    }
    handleNewBooking(){
        const booking = this.props.store.getIn(["booking"]).toObject();
        if(booking.fullname==""){
            alert("请填写姓名");
            return;
        }
        if(booking.mobile==""){
            alert("请填写联系方式");
            return;
        }
        if(booking.bookingDate==""){
            alert("请选择预约日期");
            return;
        }
        if(booking.bookingHour=="" || booking.bookingMinutes==""){
            alert("请选择预约时间");
            return;
        }
        if(booking.resourceId=="-1"){
            alert("请选择餐台类型");
            return;
        }

        this.props.dispatch(BookingActions.Actions.NewBooking({
            id: booking.id,
            resourceId: booking.resourceId,
            resourceName: booking.resourceName,
            bookingDate: moment().format(booking.bookingDate),
            bookingTime: booking.bookingHour+":"+booking.bookingMinutes,
            fullname: booking.fullname,
            mobile: booking.mobile,
            memo: booking.memo,
            gender: booking.male?"MALE":"FEMALE",
            bookingStatus : "Unconfirmed"
        }));
    }
    handleCancelNewBooking(){
        this.props.dispatch(UIActions.Actions.OpenListBookings())
    }
    onConfirmBookingSuccessed(){
        this.props.dispatch(UIActions.Actions.OpenListBookings());
    }
}

