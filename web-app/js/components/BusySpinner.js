'use strict';

import React from 'react';
import styles from '../less/app.less';

export default class extends React.Component {
    render() {
        if (this.props.show) {
            return (
                <div className={styles.spinner}>
                    <div className={styles.icon}/>
                </div>
            );
        } else {
            return null;
        }
    }
}