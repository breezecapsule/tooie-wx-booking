'use strict';

import React from 'react';
import {Grid, Col, Row, Button, Modal, Input, Alert, Panel} from 'react-bootstrap';
import BusySpinner from './BusySpinner';
import cn from 'classnames';
import BookingActions from '../actions/BookingActions';
import NewBooking from '../components/NewBooking';
import ModifyBooking from '../components/ModifyBooking';
import ListBooking from '../components/ListBooking';
import ListInvalidBooking from '../components/ListInvalidBooking';
import BookingDetails from '../components/BookingDetails';
import styles from '../less/app.less';
import queryString from 'query-string';
import _ from 'underscore';
import moment from 'moment';

export default class extends React.Component {
    componentWillMount(){
        const parsed = queryString.parse(location.search);
        this.props.dispatch(BookingActions.Actions.Bootstrap(parsed));
    }
    render() {
        let child = null;
        switch (this.props.store.getIn(["ui", "page"])) {
            case "list":
                child = <ListBooking dispatch={this.props.dispatch} store={this.props.store}/>;
                break;
            case "invalidList":
                child = <ListInvalidBooking dispatch={this.props.dispatch} store={this.props.store}/>;
                break;
            case "new":
                child = <NewBooking dispatch={this.props.dispatch} store={this.props.store}/>;
                break;
            case "details":
                child = <BookingDetails dispatch={this.props.dispatch} store={this.props.store}/>;
                break;
            case "modify":
                child = <ModifyBooking dispatch={this.props.dispatch} store={this.props.store}/>;
                break;
            default:
                child = <div>Error</div>;
        }

        return (
            <Grid fluid>
                <Row>
                    <Col xs={12}>
                        {child}
                    </Col>
                </Row>
                <BusySpinner show={this.props.store.getIn(['ui', 'busy'])}/>
            </Grid>
        );
    }
}

