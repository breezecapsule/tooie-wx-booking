var path = require('path');
var webpack = require('webpack');
var node_modules = path.resolve(__dirname, 'node_modules');
//
module.exports = {
    entry: {
        wxbooking: [
            'webpack-dev-server/client?https://localhost:3002',
            'webpack/hot/only-dev-server',
            "./main"
        ]
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: "[name].bundle.js",
        publicPath: 'https://localhost:3002/dev/'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({'process.env.NODE_ENV': '"development"', '__DEV__': true })
    ],

    resolve: {
        modulesDirectories: ["web_modules", "node_modules"],
        extensions: ['', '.js']
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['react-hot', 'babel-loader'],
                include: path.join(__dirname, './'),
                exclude: path.resolve(__dirname, "node_modules")
            },
            { test: /\.less$/, loader: "style-loader!css-loader?modules!less"
            },
            {
                test: /\.css$/, loader: "style-loader!css-loader?modules",
                exclude: path.resolve(__dirname, "node_modules")
            },
            {
                test: /\.css$/, loader: "style-loader!css-loader?",
                include: path.resolve(__dirname, "node_modules")
            },
            {test: /\.woff$/, loader: "url-loader?limit=10000&minetype=application/font-woff"},
            {test: /\.woff2$/, loader: "url-loader?limit=10000&minetype=application/font-woff"},
            {test: /\.ttf$/, loader: "file-loader"},
            {test: /\.eot$/, loader: "file-loader"},
            {test: /\.svg$/, loader: "file-loader"},
            {test: /\.png/, loader: "file-loader"},
            {test: /\.gif$/, loader: "file-loader"},
            {test: /\.jpg$/, loader: "file-loader"}
        ]
    }
};
