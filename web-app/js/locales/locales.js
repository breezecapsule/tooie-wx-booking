export default {
    "en": {
        translation: require("json!./en-US/translation.json")
    },
    "en_US": {
        translation: require("json!./en-US/translation.json")
    },
    "zh_CN": {
        translation: require("json!./zh-CN/translation.json")
    },
    "zh_TW": {
        translation: require("json!./zh-TW/translation.json")
    }
}